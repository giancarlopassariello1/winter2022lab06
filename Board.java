public class Board {
  
  private Die p1;
  private Die p2;
  private boolean[] closedTiles;
  
  public Board () {
   this.p1 = new Die();
   this.p2 = new Die();
   this.closedTiles = new boolean[12];
  }
  public String toString() {
   String cT = "";
   int x = 1;
   for (int i = 0; i < this.closedTiles.length; i++) {
     if (closedTiles[i] == false) {
       cT = cT+x+" ";
     } else {
       cT = cT+"X"+" ";
     }
    x++; 
   }
   return cT;
 }
 
 public boolean playATurn () {
   this.p1.roll();
   this.p2.roll();
   System.out.println(this.p1.getPips());
   System.out.println(this.p2.getPips());
   int sumOfPips = this.p1.getPips() + this.p2.getPips();
   
   if (this.closedTiles[sumOfPips-1] == false) {
     this.closedTiles[sumOfPips-1] = true;
     System.out.println("Closing tile: "+sumOfPips);
     return false;
   } else {
     System.out.println("The tile at this position is already shut");
     return true;
   }
 }
}
 
 