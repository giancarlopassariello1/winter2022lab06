import java.util.Random;

public class Die {
 
 private int pips;
 private Random rand;
 
 public Die () {
 this.pips = 1;
 this.rand = new Random();
 }
 
 public int getPips () {
 return this.pips;
 }
 
 public void roll () {
 this.pips = this.rand.nextInt(6)+1;
 }
 
 public String toString () {
 return "pips= "+this.pips;
 }
 
}
